package ntnu.idi.bidata.k2g9;

import ntnu.idi.bidata.k2g9.hospitalUnits.Department;
import ntnu.idi.bidata.k2g9.personnel.Employee;
import ntnu.idi.bidata.k2g9.personnel.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests.
 * In setup, which is ran before every test, dummyEmployee and dummyPatient are added to the test department.
 * patientTest and employeeTest are initialized, but not added to the department in setup.
 * Tests are done by trying to remove or add either one of the added persons, or vice versa.
 * The tests are nested in classes representing what method they are testing.
 */
public class HospitalTest {
    Department emergencyTest;
    Patient patientTest;
    Patient dummyPatient;
    Employee employeeTest;
    Employee dummyEmployee;

    @BeforeEach
    public void setUp(){
        emergencyTest = new Department("Emergency Test");
        patientTest = new Patient("test1", "test1", "12345678910");
        dummyPatient = new Patient("dummy", "patient", "21345678910");
        employeeTest = new Employee("test2", "test2", "10987654321");
        dummyEmployee = new Employee("dummy", "employee", "22345678910");
        emergencyTest.addEmployee(dummyEmployee);
        emergencyTest.addPatient(dummyPatient);
    }

    @Nested
    @DisplayName("Tests for the method addEmployee")
    class AddEmployee{
        @Test
        public void testAddingEmployeeToDepartment(){
            assertTrue(emergencyTest.addEmployee(employeeTest));
        }

        @Test
        public void testAddingAnExistingEmployee(){
            assertFalse(emergencyTest.addEmployee(dummyEmployee));
        }
    }

    @Nested
    @DisplayName("Tests for the method addPatients")
    class AddPatients{

        @Test
        public void testAddingAnExistingPatient(){
            assertFalse(emergencyTest.addPatient(dummyPatient));
        }

        @Test
        public void testAddingNotExistingPatientsToDepartment(){
            assertTrue(emergencyTest.addPatient(patientTest));
        }
    }

    @Nested
    @DisplayName("Tests for the method removePerson")
    class RemovePerson{
        @Test
        public void testRemovingExistingEmployeeFromDepartment() throws RemoveException{
            assertTrue(emergencyTest.removePerson(dummyEmployee));
        }

        @Test
        public void testRemovingExistingPatientFromDepartment() throws RemoveException {
            assertTrue(emergencyTest.removePerson(dummyPatient));
        }

        @Test
        public void testRemovingNotExistingEmployeeFromDepartment(){
            assertThrows(RemoveException.class,() -> emergencyTest.removePerson(employeeTest));
        }

        @Test
        public void testRemovingNotExistingPatientFromDepartment(){
            assertThrows(RemoveException.class, () -> emergencyTest.removePerson(patientTest));
        }
    }
}
