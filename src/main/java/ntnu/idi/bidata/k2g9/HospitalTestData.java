package ntnu.idi.bidata.k2g9;

import ntnu.idi.bidata.k2g9.hospitalUnits.Department;
import ntnu.idi.bidata.k2g9.hospitalUnits.Hospital;
import ntnu.idi.bidata.k2g9.personnel.*;

public final class HospitalTestData {
    private HospitalTestData() {
        // not called
    }
    /**
     * @param hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        // Add some departments
        Department emergency = new Department("er");
        emergency.addEmployee(new Employee("OddEven", "Primtallet", "75702464840"));
        emergency.addEmployee(new Employee("Huppasahn", "DelFinito", "50039848751"));
        emergency.addEmployee(new Employee("Rigmor", "Mortis", "46189354687"));
        emergency.addEmployee(new GeneralPractitioner("Inco", "Gnito", "69964971628"));
        emergency.addEmployee(new Surgeon("Inco", "Gnito", "35559204039"));
        emergency.addEmployee(new Nurse("Nina", "Teknologi", "27848977873"));
        emergency.addEmployee(new Nurse("Ove", "Ralt", "04118369005"));
        emergency.addPatient(new Patient("Inga", "Lykke", "21108656312"));
        emergency.addPatient(new Patient("Ulrik", "Smål", "50387948811"));
        hospital.addDepartment(emergency);
        Department childrenPolyclinic = new Department("pa");
        childrenPolyclinic.addEmployee(new Employee("Salti", "Kaffen", "18455262593"));
        childrenPolyclinic.addEmployee(new Employee("Nidel V.", "Elvefølger", "37251584880"));
        childrenPolyclinic.addEmployee(new Employee("Anton", "Nym", "01895249712"));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene", "Sis", "30062146580"));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna", "Na", "91050505193"));
        childrenPolyclinic.addEmployee(new Nurse("Nora", "Toriet", "23126955644"));
        childrenPolyclinic.addPatient(new Patient("Hans", "Omvar", "93646406622"));
        childrenPolyclinic.addPatient(new Patient("Laila", "La", "29464600147"));
        childrenPolyclinic.addPatient(new Patient("Jøran", "Drebli", "61422571283"));
        hospital.addDepartment(childrenPolyclinic);
    }
}
