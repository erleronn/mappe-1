package ntnu.idi.bidata.k2g9.personnel;

/**
 * The class General practitioner, inheriting from Doctor.
 * @see Doctor
 */
public class GeneralPractitioner extends Doctor{

    /**
     * Constructor for the class GeneralPractitioner.
     * @param givenName
     * @param surname
     * @param socialSecurityNumber
     */
    public GeneralPractitioner(String givenName, String surname, String socialSecurityNumber) {
        super(givenName, surname, socialSecurityNumber);
    }

    /**
     * Inherited method from doctor. Enables general practitioners to set diagnoses.
     * @param patient The patient to be diagnosed.
     * @param diagnosis The diagnose.
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }

    /**
     * Modified toString.
     * @return Displays that the employee is a general practitioner.
     */
    @Override
    public String toString(){
        return "\nGENERAL PRACTITIONER\nName: " + getSurname() + ',' + getGivenName() + "\n" + "Social security number: " + getSocialSecurityNumber();

    }
}
