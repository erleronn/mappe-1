package ntnu.idi.bidata.k2g9.personnel;

import ntnu.idi.bidata.k2g9.hospitalUnits.Department;

import java.util.Objects;

/**
 *The abstract superclass Person.
 *The class represents a person in the HR-system.
 */
public abstract class Person {
    private String givenName;
    private String surname;
    private String socialSecurityNumber;


    /**
     * Constructor for class Person.
     * @param givenName the given name of the person. Can not be null or blank.
     * @param surname the surname of the person. Can not be null or blank.
     * @param socialSecurityNumber the social security number of the person. Can not be null, has to be 11 digits.
     */
    public Person(String givenName, String surname, String socialSecurityNumber) {

        if (givenName == null || givenName.isBlank()) {
            throw new IllegalArgumentException();
        }
        this.givenName = givenName;

        if (surname == null || surname.isBlank()) {
            throw new IllegalArgumentException();
        } else {
            this.surname = surname;
        }

        if (socialSecurityNumber == null || !(socialSecurityNumber.length() == 11)) {
            throw new IllegalArgumentException();
        }
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * @return givenName
     */
    public String getGivenName() {
        return givenName;
    }

    /**
     * @param givenName
     */
    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    /**
     * @return surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     *
     * @param surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     *
     * @return socialSecurityNumber
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }


    /**
     * toString method.
     * @return Returns a string that neatly presents the data of the person.
     */
    @Override
    public String toString() {
        return "Name: " + surname + ',' + givenName + "/n" + "Social security number: " + socialSecurityNumber;
    }

    /**
     * Custom equals.
     * Is used in Department.java
     * @see Department
     * @param o Object being compared.
     * @return Boolean value of the equality.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return getGivenName().equals(person.getGivenName()) && getSurname().equals(person.getSurname()) && getSocialSecurityNumber().equals(person.getSocialSecurityNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGivenName(), getSurname(), getSocialSecurityNumber());
    }
}
