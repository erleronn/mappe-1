package ntnu.idi.bidata.k2g9.personnel;

/**
 * The abstract class Doctor, inheriting from Employee.
 * @see Employee
 */
public abstract class Doctor extends Employee {
    /**
     * Constructor for the class Doctor.
     * @param givenName
     * @param surname
     * @param socialSecurityNumber
     */
    public Doctor(String givenName, String surname, String socialSecurityNumber) {
        super(givenName, surname, socialSecurityNumber);
    }

    /**
     * The method giving doctors the privilege to set diagnoses.
     * @param patient The patient to be diagnosed.
     * @param diagnosis The diagnose.
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
