package ntnu.idi.bidata.k2g9.personnel;

/**
 * The class Nurse, inheriting from Employee.
 * @see Employee
 */
public class Nurse extends Employee{

    /**
     * Constructor for class Nurse.
     * @param givenName
     * @param surname
     * @param socialSecurityNumber
     */
    public Nurse(String givenName, String surname, String socialSecurityNumber) {
        super(givenName, surname, socialSecurityNumber);
    }

    /**
     * Modified toString.
     * @return Displays that the person is a nurse.
     */
    @Override
    public String toString() {
        return "\nNURSE\nName: " + getSurname() + ',' + getGivenName() + "\n" + "Social security number: " + getSocialSecurityNumber();
    }
}
