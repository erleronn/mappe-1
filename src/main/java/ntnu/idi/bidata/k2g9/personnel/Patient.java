package ntnu.idi.bidata.k2g9.personnel;

/**
 * The class Patient, inheriting from Person, and implementing Diagnosable.
 * @see Patient
 * @see Diagnosable
 */
public class Patient extends Person implements Diagnosable {

    private String diagnosis = "No diagnosis set";

    /**
     * Constructor for class Patient
     * @param givenName
     * @param surname
     * @param socialSecurityNumber
     */
    public Patient(String givenName, String surname, String socialSecurityNumber) {
        super(givenName, surname, socialSecurityNumber);
    }

    /**
     * @return diagnosis
     */
    protected String getDiagnosis(){
        return diagnosis;
    }

    /**
     * Method to be called by a doctor to set diagnosis.
     * @param diagnosis The diagnosis to be added.
     */
    public void setDiagnosis(String diagnosis){
        this.diagnosis = diagnosis;
    }

    /**
     * Modified toString.
     * @return Displays that the person is a patient.
     */
    @Override
    public String toString() {
        return "\nPATIENT\nName: " + getSurname() + ',' + getGivenName() + "\n" + "Social security number: " + getSocialSecurityNumber() + "\nDiagnosis: " + diagnosis + "\n";
    }
}
