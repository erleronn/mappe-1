package ntnu.idi.bidata.k2g9.personnel;

/**
 * The class Employee, inheriting from Person
 * @see Person
 */
public class Employee extends Person {

    /**
     * Constructor for the class Employee.
     * @param givenName
     * @param surname
     * @param socialSecurityNumber
     */
    public Employee(String givenName, String surname, String socialSecurityNumber) {
        super(givenName, surname, socialSecurityNumber);
    }

    /**
     * Modified toString
     * @return Displays that the person is an employee.
     */
    @Override
    public String toString(){
        return "\nEMPLOYEE\nName: " + getSurname() + ',' + getGivenName() + "\n" + "Social security number: " + getSocialSecurityNumber();
    }
}
