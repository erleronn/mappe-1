package ntnu.idi.bidata.k2g9.personnel;

/**
 * Class Surgeon, inheriting from Doctor.
 */
public class Surgeon extends Doctor {

    /**
     * Constructor for the class Surgeon.
     * @param givenName
     * @param surname
     * @param socialSecurityNumber
     */
    public Surgeon(String givenName, String surname, String socialSecurityNumber) {
        super(givenName, surname, socialSecurityNumber);
    }

    /**
     * Inherited method from doctor. Enables surgeons to set diagnoses.
     * @param patient The patient to be diagnosed.
     * @param diagnosis The diagnose.
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }

    /**
     * Modified toString
     * @return Displays that the person is a surgeon.
     */
    @Override
    public String toString(){
        return "\nSURGEON\nName: " + getSurname() + ',' + getGivenName() + "\n" + "Social security number: " + getSocialSecurityNumber();

    }
}
