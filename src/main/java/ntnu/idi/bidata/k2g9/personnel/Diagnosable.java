package ntnu.idi.bidata.k2g9.personnel;

/**
 * Interface Diagnosable.
 * To be implemented by Patient.java.
 * @see Patient
 */
public interface Diagnosable {
    /**
     * The method to be called by doctors.
     * @param diagnosis The diagnosis to be added.
     */
    void setDiagnosis(String diagnosis);
}
