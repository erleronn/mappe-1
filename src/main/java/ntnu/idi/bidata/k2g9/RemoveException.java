package ntnu.idi.bidata.k2g9;

import java.io.Serial;

/**
 * Class RemoveException, inheriting from Exception.
 */
public class RemoveException extends Exception{
    /**
     * Constructor for the class RemoveException.
     * This constructor takes no parameters.
     */
    public RemoveException(){
        super("The person does not exist in the register!\n");
    }

    /**
     * Constructor for the class RemoveException.
     * @param message Message to be printed upon catching an error.
     */
    public RemoveException(String message){ super(message);}

    @Serial
    private static final long serialVersionUID = 1L;
}
