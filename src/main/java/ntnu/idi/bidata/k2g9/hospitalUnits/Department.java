package ntnu.idi.bidata.k2g9.hospitalUnits;

import ntnu.idi.bidata.k2g9.RemoveException;
import ntnu.idi.bidata.k2g9.personnel.Employee;
import ntnu.idi.bidata.k2g9.personnel.Patient;
import ntnu.idi.bidata.k2g9.personnel.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The class Department.
 */
public class Department {

    private String departmentName;
    private final List<Employee> employees;
    private final List<Patient> patients;

    /**
     * Constructor for class Department.
     * I chose to use ArrayLists because of the dynamic size of the lists.
     * @param departmentName name of the department.
     */
    public Department(String departmentName) {
        employees = new ArrayList<>();
        patients = new ArrayList<>();
        if(departmentName == null || departmentName.isBlank()){
            throw new IllegalArgumentException();
        }
        this.departmentName = departmentName;
    }

    /**
     * @return departmentName
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * @param departmentName
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * @return employees
     */
    public List<Employee> getEmployees(){
        return employees;
    }

    /**
     * @return patients
     */
    public List<Patient> getPatients() { return patients; }

    /**
     * Checks if the employee is already in the system before adding to the list.
     * Only works with custom equals in Person.
     * @see Person
     * @param newEmployee The employee to be added.
     * @return Boolean depending on whether the adding went through or not.
     */
    public boolean addEmployee(Employee newEmployee){
        boolean isSuccessful = false;
        try {
            if(employees.contains(newEmployee)){
                throw new IllegalArgumentException();
            } else {
                employees.add(newEmployee);
                isSuccessful = true;
            }
        } catch (IllegalArgumentException i){
            System.out.println("This employee is already in the system!");
        }
        return isSuccessful;
    }

    /**
     * Checks if a patient is already in the system, before adding it to the list.
     * I have chosen to use aggregation because I do not need the personnel to exist outside of the departments.
     * @param newPatient The patient to be added.
     * @return Boolean depending on whether the adding went through or not.
     */
    public boolean addPatient(Patient newPatient){
        boolean isSuccessful = false;
        try {
            if(patients.contains(newPatient)){
                throw new IllegalArgumentException();

            } else {
                patients.add(newPatient);
                isSuccessful = true;
            }
        } catch (IllegalArgumentException i){
            System.out.println("This patient is already in the system!");
        }
        return isSuccessful;
    }


    /**
     * Method checks whether the person to be removed is an employee or a patient.
     * It then iterates through and checks that the list contains the person, before removing it.
     * @param person The person to be removed.
     * @return Boolean depending on whether the removal went through or not.
     * @throws RemoveException Custom exception thrown by trying to remove a person who is not in the system.
     * @see RemoveException
     */
    public boolean removePerson(Person person) throws RemoveException {
        boolean success = false;
        if (person instanceof Employee) {
            if (employees.contains(person)) {
                employees.remove(person);
                success = true;
            } else {
                throw new RemoveException();

            }

        } else if (person instanceof Patient) {
            if (patients.contains(person)) {
                patients.remove(person);
                success = true;
            } else {
                throw new RemoveException();
            }
        }
        return success;
    }

    /**
     * Custom equals for comparing departments.
     * @param o The department to compare this department with.
     * @return Boolean value of the equality.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return departmentName.equals(that.departmentName) && Objects.equals(employees, that.employees) && Objects.equals(patients, that.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    /**
     * Displays the info of the employees in a given department.
     * @return StringBuilder containing the employees.
     */
    public String displayEmployees(){
        StringBuilder build = new StringBuilder();
        build.append("Employees in department: ").append(departmentName).append("\n");
        for(Employee emp : employees){
            build.append(emp).append("\n");
        }
        build.append("\n\n");
        return build.toString();
    }

    /**
     * Displays the info of the patients in a given department.
     * @return StringBuilder containing the patients.
     */
    public String displayPatients(){
        StringBuilder builder = new StringBuilder();
        builder.append("Patients in department: ").append(departmentName).append("\n");
        for(Patient pat : patients){
            builder.append(pat);
        }
        builder.append("\n\n");
        return builder.toString();
    }
}
