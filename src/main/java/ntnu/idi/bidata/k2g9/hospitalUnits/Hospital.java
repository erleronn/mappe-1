package ntnu.idi.bidata.k2g9.hospitalUnits;

import java.util.ArrayList;
import java.util.List;

/**
 * The class Hospital.
 */
public class Hospital {
    private final String hospitalName;

    private final List<Department> departments;

    /**
     * Constructor for the class Hospital.
     * I have chosen to use ArrayList because of the dynamic size of the list.
     * An alternative would have been to use hashMap, with the department name as the key.
     * @param hospitalName Name of the hospital. Can not be null or blank.
     */
    public Hospital(String hospitalName){
        if(hospitalName == null || hospitalName.isBlank()){
            throw new IllegalArgumentException();
        }
        this.hospitalName = hospitalName;
        departments = new ArrayList<>();
    }

    /**
     * @return hospitalName
     */
    public String getHospitalName(){
        return hospitalName;
    }

    /**
     * @return departments
     */
    public List<Department> getDepartments() {
        return departments;
    }

    /**
     * Checks if the list of departments already contains the department to be added.
     * I have used aggregation because it does not make sense for the departments to exist
     * outside of the hospital object.
     * @param newDepartment The department to be added.
     * @throws IllegalArgumentException Thrown if the list already contains the new department.
     */
    public void addDepartment(Department newDepartment) throws IllegalArgumentException{
        try{
            if(!(departments.contains(newDepartment))){
                departments.add(newDepartment);
            }
        } catch (IllegalArgumentException i){
            System.out.println(i.getMessage());
        }

    }

    /**
     * @return String displaying the hospital name and its departments.
     */
    public String toString(){
        return "Hospital name:" + hospitalName + "/nDepartments: " + departments;
    }
}
