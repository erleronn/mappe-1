package ntnu.idi.bidata.k2g9;
import ntnu.idi.bidata.k2g9.hospitalUnits.Department;
import ntnu.idi.bidata.k2g9.hospitalUnits.Hospital;
import ntnu.idi.bidata.k2g9.personnel.*;

import java.util.Scanner;

public class HospitalClient{
    public static void main(String[] args){
        Hospital theHospital = new Hospital("The Hospital");
        HospitalTestData.fillRegisterWithTestData(theHospital);
        int counter;

        while(true) {
            Scanner s = new Scanner(System.in);
            System.out.println("Welcome to the hospital text client!\nWhat would you like to do?");
            System.out.println("----------------------------------");
            printOptions();
            System.out.println("----------------------------------");
            System.out.println("\nEnter a number.");
            int input = Integer.parseInt(s.nextLine());
            counter = 0;

            switch (input) {
                case 1 -> {
                    System.out.println("Input the desired department:");
                    Scanner in = new Scanner(System.in);
                    String department = in.nextLine();
                    for (Department dep : theHospital.getDepartments()) {
                        if (dep.getDepartmentName().equalsIgnoreCase(department)) {
                            System.out.println(dep.displayEmployees());
                            System.out.println(dep.displayPatients());
                            counter++;
                        }
                    }
                    if (counter == 0) {
                        throw new IllegalArgumentException();
                    }
                }
                case 2 -> {
                    Scanner case2 = new Scanner(System.in);
                    System.out.println("Enter the given name of the employee:");
                    String givenName = case2.nextLine();
                    System.out.println("Enter the surname of the employee:");
                    String surname = case2.nextLine();
                    System.out.println("Enter the social security number of the employee:");
                    String ssn = case2.nextLine();
                    System.out.println("What position does the employee have?");
                    String position = case2.nextLine();
                    Employee newEmployee;
                    if (position.equalsIgnoreCase("employee")) {
                        newEmployee = new Employee(givenName, surname, ssn);
                    } else if (position.equalsIgnoreCase("nurse")) {
                        newEmployee = new Nurse(givenName, surname, ssn);
                    } else if (position.equalsIgnoreCase("general practitioner")) {
                        newEmployee = new GeneralPractitioner(givenName, surname, ssn);
                    } else if (position.equalsIgnoreCase("surgeon")) {
                        newEmployee = new Surgeon(givenName, surname, ssn);
                    } else {
                        throw new IllegalArgumentException();
                    }
                    System.out.println("Enter the department the employee should be added to:");
                    String dept = case2.nextLine();
                    for (Department d : theHospital.getDepartments()) {
                        if (d.getDepartmentName().equalsIgnoreCase(dept)) {
                            try {
                                if (d.addEmployee(newEmployee)) {
                                    System.out.println("Employee successfully added!\n");
                                }
                            } catch (IllegalArgumentException i) {
                                System.out.println("Unsuccessful! Employee not added.");
                            }
                        }
                    }
                }
                case 3 -> {
                    Scanner case3 = new Scanner(System.in);
                    System.out.println("Enter the given name of the patient:");
                    String givenNameP = case3.nextLine();
                    System.out.println("Enter the surname of the patient:");
                    String surnameP = case3.nextLine();
                    System.out.println("Enter the social security number of the patient:");
                    String ssnP = case3.nextLine();
                    Patient newPatient = new Patient(givenNameP, surnameP, ssnP);
                    System.out.println("Enter the department the patient should be added to:");
                    String deptP = case3.nextLine();
                    for (Department d : theHospital.getDepartments()) {
                        if (d.getDepartmentName().equalsIgnoreCase(deptP)) {
                            try {
                                if (d.addPatient(newPatient)) {
                                    System.out.println("Patient successfully added!\n");
                                }
                            } catch (IllegalArgumentException e) {
                                System.out.println("Unsuccessful! Patient not added.");
                            }
                        }
                    }
                }
                case 4 -> {
                    Scanner case4 = new Scanner(System.in);
                    System.out.println("Enter the given name of the employee:");
                    String givenNameRemove = case4.nextLine();
                    System.out.println("Enter the surname of the employee:");
                    String surnameRemove = case4.nextLine();
                    System.out.println("Enter the social security number of the employee:");
                    String ssnRemove = case4.nextLine();
                    System.out.println("Enter the department the patient should be removed from:");
                    String removeDepartment = case4.nextLine();
                    Employee removeEmployee = new Employee(givenNameRemove, surnameRemove, ssnRemove);
                    try {
                        for (Department depart : theHospital.getDepartments()) {
                            if (depart.getDepartmentName().equalsIgnoreCase(removeDepartment)) {
                                if (depart.removePerson(removeEmployee)) {
                                    System.out.println("Patient successfully removed!\n");
                                }
                            }
                        }
                    } catch (RemoveException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 5 -> {
                    Scanner case5 = new Scanner(System.in);
                    System.out.println("Enter the given name of the patient:");
                    String givenNameRemoveP = case5.nextLine();
                    System.out.println("Enter the surname of the patient:");
                    String surnameRemoveP = case5.nextLine();
                    System.out.println("Enter the social security number of the patient:");
                    String ssnRemoveP = case5.nextLine();
                    Patient removePatient = new Patient(givenNameRemoveP, surnameRemoveP, ssnRemoveP);
                    System.out.println("Enter the department the patient should be removed from:");
                    String removeDepart = case5.nextLine();
                    try {
                        for (Department depart : theHospital.getDepartments()) {
                            if (depart.getDepartmentName().equalsIgnoreCase(removeDepart)) {
                                if (depart.removePerson(removePatient)) {
                                    System.out.println("Patient successfully removed!\n");
                                }
                            }
                        }
                    } catch (RemoveException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 6 -> {
                    System.out.println("Goodbye!\nThank you for using the hospital client!");
                    return;
                }
                default -> throw new IllegalStateException("Unexpected value: " + input);
            }
        }
    }

    private static void printOptions(){
        String[] options = {"1: Display employees and patients of a department","2: Add an employee",
                "3: Add a patient","4: Remove an employee","5: Remove a patient","6: Exit"};
        for (String string : options){
            System.out.println(string);
        }
    }
}